import Vue from 'vue'
import VueTyper from 'vue-typer'

import VueTypedJs from 'vue-typed-js'

Vue.use(VueTyper)

Vue.use(VueTypedJs)
